# boothshell

Boothshell is a simple implementation of a shell in the C language. It is a basic shell. That is: read, parse, fork, exec, and wait. Since its very small, it has many limitations.